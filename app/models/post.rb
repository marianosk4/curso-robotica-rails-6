class Post < ApplicationRecord
  has_rich_text :content
  has_one_attached :imagen

def next
    Post.where("id > ?", id).order(id: :asc).limit(1).first
end

def prev
     Post.where("id < ?", id).order(id: :desc).limit(1).first
end




end
